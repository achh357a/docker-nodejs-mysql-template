'use strict';

const restify = require('restify');
const swaggerJSDoc = require('swagger-jsdoc');
const config = require('./config.json')[process.env.NODE_ENV || 'local'];
const winston = require('winston');
const os = require('os');
const mysql = require('mysql');
const uuid = require('uuid/v4');

const pool = mysql.createPool({
  host: 'mysql',
  port: '3306',
  user: 'root',
  password: 'some_password',
  database: 'db',
});

// Mandatory Configuration
const apiServiceName = 'mysql';
const apiBasePath = '/mysql';

// Create global logger objectdocke
const logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      level: 'info',
      timestamp: new Date().toString(),
      json: true,
    }),
  ],
});

// Create swaggerSpec object
const swaggerSpec = swaggerJSDoc(config.swagger);

// Create restify server object
const server = restify.createServer({
  name: apiServiceName,
});

server.use(restify.bodyParser({ mapParams: false })); // mapped in req.body

// Get IP Address of Server
function getIP() {
  return new Promise((resolve, reject) => {
    const adrs = os.networkInterfaces();
    let count = 0;
    Object.keys(adrs).forEach((key) => {
      adrs[key].forEach((item) => {
        if (item.internal === false && item.family === 'IPv4') {
          resolve(item.address);
          count += 1;
        }
      });
    });
    if (count === 0) {
      reject(`No IP found for ${apiServiceName}`);
    }
  });
}

/**
 * @swagger
 * definitions:
 *    schemas:
 *      type: object
 *      required:
 *      - name
 *      properties:
 *        name:
 *          type: string
 */

/**
 * @swagger
 * definitions:
 *   HelloWorldResponse:
 *    properties:
 *       hello:
 *         type: string
 * /hello-world:
 *   get:
 *     tags:
 *       - helloworld
 *     description: Returns hello world
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponse'
 */
server.post(`${apiBasePath}/tables`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      let column = '';
      const table = req.body.tableName;
      const columns = req.body.tableColumns;
      for (let i = 0; i < columns.length; i++) {
        column += `${columns[i].columnName} ${columns[i].columnType},`
      }
      column = column.slice(0, -1);
      const tableCreate = `CREATE TABLE ${table} (uuid VARCHAR(255),${column})`
      connection.query(tableCreate, (err, result) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else {
          connection.release();
          res.send(200, result);
          next();
        }
        return true;
      });
    }
  });
});

server.get(`${apiBasePath}/tables/:table/records`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      const table = req.params.table;
      connection.query(`SELECT * FROM ${table}`, (err, result, fields) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else if (result.length < 1) {
          connection.release();
          const message = {
            message: `No records found in specified table: ${table}`
          }
          res.send(200, message);
          next();
        } else {
          connection.release();
          res.send(200, result);
          next();
        }
        return true;
      });
    }
  });
});

server.post(`${apiBasePath}/tables/:table/records`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      const table = req.params.table;
      const columnName = req.body.columnName;
      let insertUuid = uuid();
      insertUuid = insertUuid.replace(/-/g, '');
      let finalArr = [];
      let insert = req.body.values;
      insert.unshift(insertUuid);
      finalArr[0] = insert;
      let keys = '';
      for (let i = 0; i < columnName.length; i++) {
        keys += `${columnName[i]},`
      }
      keys = keys.slice(0, -1)
      connection.query(`INSERT INTO ${table} (uuid,${keys}) VALUES ?`, [finalArr], (err, result) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else {
          connection.release();
          const message = {
            message: `New record with uuid ${insertUuid} created in table: ${table}`
          }
          res.send(200, message);
          next();
        }
      });
    }
  });
});

server.get(`${apiBasePath}/tables/:table/records/:uuid`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      const table = req.params.table;
      const id = req.params.uuid;
      connection.query(`SELECT * FROM ${table} WHERE \`uuid\`=\'${id}\'`, (err, result, fields) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else if (result.length < 1) {
          connection.release();
          const message = {
            message: `Record ${id} not found in table: ${table}`
          }
          res.send(200, message);
          next();
        } else {
          connection.release();
          res.send(200, result);
          next();
        }
        return true;
      });
    }
  });
});

server.del(`${apiBasePath}/tables/:table/records/:uuid`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      const table = req.params.table;
      const id = req.params.uuid;
      connection.query(`SELECT * FROM ${table} WHERE \`uuid\`=\'${id}\'`, (err, result, fields) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else if (result.length < 1) {
          connection.release();
          const message = {
            message: `Record ${id} not found in table: ${table}`
          }
          res.send(200, message);
          next();
        } else {
          //delete record here
          connection.query(`DELETE FROM ${table} WHERE \`uuid\`=\'${id}\'`, (err, result, fields) => {
            if (err) {
              connection.release();
              res.send(500, `${err}`);
              next();
            } else {
              const message = {
                message: `Record ${id} has been deleted from table: ${table}`
              }
              connection.release();
              res.send(200, message);
              next();
            }
          });
        }
        return true;
      });
    }
  });
});

server.put(`${apiBasePath}/tables/:table/records/:uuid`, (req, res, next) => {
  pool.getConnection((error, connection) => {
    if (error) {
      res.send(500, `Connection error: ${error}`);
      next();
    } else {
      const table = req.params.table;
      const id = req.params.uuid;
      connection.query(`SELECT * FROM ${table} WHERE \`uuid\`=\'${id}\'`, (err, result, fields) => {
        if (err) {
          connection.release();
          res.send(500, `${err}`);
          next();
        } else if (result.length < 1) {
          connection.release();
          const message = {
            message: `Record ${id} not found in table: ${table}`
          }
          res.send(200, message);
          next();
        } else {
          const columnName = req.body.columnName;
          let insert = req.body.values;
          let keys = '';
          for (let i = 0; i < columnName.length; i++) {
            keys += `\`${columnName[i]}\` = ?, `
          }
          keys = keys.slice(0, -2)
          connection.query(`UPDATE ${table} SET ${keys} WHERE \`uuid\` = \'${id}\'`, insert, (err, result, fields) => {
            if (err) {
              connection.release();
              res.send(500, `${err}`);
              next();
            } else {
              const message = {
                message: `Record ${id} has been updated from table: ${table}`
              }
              connection.release();
              res.send(200, message);
              next();
            }
          });
        }
        return true;
      });
    }
  });
});

/**
 * @swagger
 * definitions:
 *   HelloWorldResponseWithId:
 *    properties:
 *       hello:
 *         type: string
 * /hello-world/test/{id}:
 *   get:
 *     tags:
 *       - helloworldwithid
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Gets your specified entry
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your sub-path input
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponseWithId'
 */

/**
 * @swagger
 * definitions:
 *   HelloWorldResponsePost:
 *    properties:
 *       hello:
 *         type: string
 *       body:
 *         type: object
 * /hello-world:
 *   post:
 *     tags:
 *       - helloworldpost
 *     parameters:
 *       - in: body
 *         name: postBody
 *         schema:
 *           $ref: "#/definitions/schemas"
 *         required: true
 *         description: Body you want to post to request
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your post body
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponsePost'
 */

/**
 * @swagger
 * definitions:
 *   HelloWorldResponsePut:
 *    properties:
 *       hello:
 *         type: string
 *       body:
 *         type: object
 * /hello-world/test/{id}:
 *   put:
 *     tags:
 *       - helloworldput
 *     parameters:
 *       - in: body
 *         name: putBody
 *         schema:
 *           $ref: "#/definitions/schemas"
 *         required: true
 *         description: Body you want to submit to change
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Record that you want to change
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your post body
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponsePut'
 */

/**
 * @swagger
 * definitions:
 *   HelloWorldResponseDelete:
 *    properties:
 *       hello:
 *         type: string
 *       entry:
 *         type: string
 * /hello-world/test/{id}:
 *   delete:
 *     tags:
 *       - helloworlddelete
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Deletes your specified entry
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your deleted entry record
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponseDelete'
 */

/**
 * @swagger
 * definitions:
 *   HealthResponse:
 *    properties:
 *       status:
 *         type: string
 *       timestamp:
 *         type: string
 * /hello-world/health:
 *   get:
 *     tags:
 *       - health
 *     description: Returns the healthcheck status of API
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON object indiciating the status of the API
 *         schema:
 *           $ref: '#/definitions/HealthResponse'
 */
server.get(`${apiBasePath}/health`, (req, res, next) => {
  const dateNow = new Date().toString();
  const responsePayload = {
    status: 'alive',
    timestamp: dateNow,
  };
  res.send(200, responsePayload);
  return next();
});

/**
 * @swagger
 * definitions:
 *   SwaggerResponse:
 *    properties:
 *       info:
 *         type: object
 *       paths:
 *         type: object
 *       definitions:
 *         type: object
 *       responses:
 *         type: object
 *       parameters:
 *         type: object
 *       securityDefinitions:
 *         type: object
 *       tags:
 *         type: object
 * /hello-world/swagger:
 *   get:
 *     tags:
 *       - swagger
 *     description: Returns a swagger definition in JSON format
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A swagger definition object in JSON
 *         schema:
 *           $ref: '#/definitions/SwaggerResponse'
 */
server.get(`${apiBasePath}/swagger`, (req, res, next) => {
  res.send(swaggerSpec);
  return next();
});

server.listen(5000, () => {
  getIP().then((ip) => {
    logger.info(`Micro Service Running on Port: ${ip}:${server.address().port}`);
  }).catch((error) => {
    logger.error(error);
  });
});
