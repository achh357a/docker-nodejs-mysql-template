# hello-world Boilerplate

A Node.js ES6 boilerplate for coding microservice that registers with Consul agent.

## Features
- **ES6**: Source codes are written in ECMAScript 2016 (ES6) and conforming to [Airbnb's eslint](https://github.com/airbnb/javascript) standards.
- **REST API**: Exposing APIs via REST using `Restify`.
- **Logging**: Central logger using `winston` that logs into json format with timestamp.
- **Service Discovery**: Automatically registers service to the Consul agent running on app server.
- **Swagger**: Exposing swagger documentation for the particular microservice in JSON format via API.
- **Environments**: Using `pm2` to run on multiple environments via a single `config.json` that consists of configurations from multiple environments.

## Pre-Requisites
* git
* Node.js v6.9.x
* yarn v0.19.x
* eslint v3.x.x

## Installation
```shell
# Clone project
$ git clone http://gitlab.prototype.apidevops.io/acnapi/template-nodejs-helloworld
$ cd template-nodejs-helloworld

# Install dependencies
$ yarn install
```

## Local Development
Before you start local development, if you do not have any Consul installed on your localhost, use the following command to create a SSH tunnel to Consul server. Please note that the service will be successfully registered and is viewable on [Consul UI](http://consul.prototype.apidevops.io) but the health check will fail because Consul server/agent cannot hit the health check API that is hosted on your localhost.
```shell
# SSH tunnel to remote Consul server
$ ssh -p 443 acnapi@bastion.prototype.apidevops.io -L 8080:10.0.1.155:8500 -N
```

Make changes to the source code and run linting. Ensure you see "Done in x.xs" without any errors before you commit or deploy to app server.

```shell
# Running linting
$ yarn lint
yarn lint v0.19.1
eslint *.js
✨  Done in 2.04s.

# Run code locally
$ yarn start
```

## App Server Deployment
```shell
# For running on DEV Node.js App Server
$ yarn run dev

# For running on PROD Node.js App Server
$ yarn run production
````
