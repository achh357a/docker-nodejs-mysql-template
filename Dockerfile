# FROM mhart/alpine-node
FROM mhart/alpine-node

WORKDIR /src
ADD . .

RUN npm install mysql uuid --save

RUN yarn install
EXPOSE 5000
CMD ["yarn", "start"]